//
//  CoreDataController.swift
//  TS
//
//  Created by Garazd on 2/5/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit
import CoreData

class CoreDataController: NSObject {
    
    static var persistentContainer: NSPersistentContainer = {
       
        let container = NSPersistentContainer(name: "TS")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
