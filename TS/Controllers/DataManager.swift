//
//  DataManager.swift
//  TS
//
//  Created by Garazd on 1/31/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import Foundation
import SWXMLHash

class DataManager {
    
    static var baseURL = Constants.URL.baseURL
    
    static func translate(from: String, to: String, text: String, completion: @escaping (String) -> Void) {
        
        let completeURL = baseURL + "?from=\(from)&to=\(to)&text=\(text)"
        
        guard let encoded = completeURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encoded) else { return }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(Constants.Authorization.key, forHTTPHeaderField: Constants.Authorization.header)
        
        let session = URLSession.shared
        _ = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else { return }
            if error != nil {
                print(error!)
            } else {
                guard let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return }
                let resultString = responseString as String
                let xml = SWXMLHash.parse(resultString)
                guard let outputText = xml["string"].element?.text else { return }
                let context = CoreDataController.persistentContainer.viewContext
                let translate = Translate(context: context)
                translate.inputText = text
                translate.outputText = outputText
                CoreDataController.saveContext()
                completion(outputText)
            }
            }.resume()
    }
}
