//
//  GifViewController.swift
//  TS
//
//  Created by Garazd on 1/31/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit
import Nuke
import NukeFLAnimatedImagePlugin
import FLAnimatedImage

let gifCollectionViewCell = "GifCollectionViewCell"
var imagesURLArray = [URL]()

class GifViewController: UIViewController {

    @IBOutlet weak var gifCollectionView: UICollectionView!
    @IBOutlet weak var gifCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gifCollectionView.delegate = self
        gifCollectionView.dataSource = self
        gifCollectionView.register(GifCollectionViewCell.self, forCellWithReuseIdentifier: gifCollectionViewCell)
        
        gifCollectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 8)
        gifCollectionViewFlowLayout.minimumInteritemSpacing = 8
        
        imagesURLArray = [
            URL(string: "https://media.giphy.com/media/26ueZnwdSw82yuGyI/giphy.gif")!,
            URL(string: "https://media.giphy.com/media/xUNen42LAAD47y0sjS/giphy.gif")!,
            URL(string: "https://media.giphy.com/media/3ov9jKsCPCityZBJ2E/giphy.gif")!,
            URL(string: "https://media.giphy.com/media/l2Sq2eZI7aFLM39aE/giphy.gif")!,
            URL(string: "https://media.giphy.com/media/3oz8xTez4VlCGc6pdC/giphy.gif")!,
            URL(string: "https://media.giphy.com/media/l3vR3xjUS1QXgUoCc/giphy.gif")!,
            URL(string: "https://media.giphy.com/media/d31wAN906IzwM6w8/giphy.gif")!,
            URL(string: "https://media.giphy.com/media/26ufo2H4vqhG8nFaE/giphy.gif")!,
            URL(string: "https://media.giphy.com/media/26ufjx48SYTCJNspW/giphy.gif")!,
        ]
    }
}

extension GifViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesURLArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = gifCollectionView.dequeueReusableCell(withReuseIdentifier: "GifCollectionViewCell", for: indexPath) as! GifCollectionViewCell
        
        cell.activityIndicator.startAnimating()
        AnimatedImage.manager.loadImage(with: Request(url: imagesURLArray[indexPath.row]), into: cell.imageView) { [weak cell] in
            cell?.activityIndicator.stopAnimating()
            cell?.imageView.handle(response: $0, isFromMemoryCache: $1)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.bounds.size.width - gifCollectionViewFlowLayout.sectionInset.left - gifCollectionViewFlowLayout.sectionInset.right
            return CGSize(width: width, height: width)
        }
    }
