//
//  TranslateViewController.swift
//  TS
//
//  Created by Garazd on 1/31/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit

class TranslateViewController: UIViewController {
    
    @IBOutlet weak var historyTableView: UITableView!
    var inputLanguage = Constants.LanguageSelector.en
    var outputLanguage = Constants.LanguageSelector.ua
    
    let context = CoreDataController.persistentContainer.viewContext
    
    var translates = [Translate]()
    {
        didSet {
            historyTableView.reloadData()
        }
    }
    
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var inputLanguageLabel: UILabel!
    @IBOutlet weak var changeLanguageButton: UIButton!
    @IBOutlet weak var outputTextView: UITextView!
    @IBOutlet weak var outputLanguageLabel: UILabel!
    @IBOutlet weak var translateButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        inputTextView.delegate = self
        historyTableView.delegate = self
        historyTableView.dataSource = self
        historyTableView.register(UITableViewCell.self, forCellReuseIdentifier: "HistoryTableViewCell")
        getData()
        setupViews()
    }
    
    func setupViews() {
        setupLanguageLabels()
        changeLanguageButton.setTitle("Change the language", for: .normal)
        translateButton.setTitle("TRANSLATE!", for: .normal)
        inputTextView.styled()
        outputTextView.styled()
        outputTextView.isEditable = false
    }
    
    func setupLanguageLabels() {
        inputLanguageLabel.text = inputLanguage.rawValue
        outputLanguageLabel.text = outputLanguage.rawValue
    }
    
    func getData() {
        do {
            translates = try context.fetch(Translate.fetchRequest())
        } catch {
            print("Fetching Failed")
        }
    }
    
    // MARK: Actions
    @IBAction func changeLanguagePressed(_ sender: UIButton) {
        let temp = inputLanguage
        inputLanguage = outputLanguage
        outputLanguage = temp
        setupLanguageLabels()
    }
    
    @IBAction func translateButtonPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if !inputTextView.text.isEmpty {
            DataManager.translate(from: inputLanguage.rawValue, to: outputLanguage.rawValue, text: inputTextView.text) { (outputText) in
                DispatchQueue.main.async {
                self.outputTextView.text = outputText
                self.getData()
                }
            }
        }
    }
}

extension TranslateViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return translates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyTableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath)
        let index = translates.count - 1 - indexPath.row
        let model = translates[index]
        if let input = model.inputText, let output = model.outputText {
        cell.textLabel?.text = ("\(input) — \(output)")
        }
        return cell
    }
}

extension TranslateViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
