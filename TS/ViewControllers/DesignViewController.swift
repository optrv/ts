//
//  DesignViewController.swift
//  TS
//
//  Created by Garazd on 1/31/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit

class DesignViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var avatarBackView: UIView!
    @IBOutlet weak var nickLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var infoStackView: UIStackView!
    @IBOutlet weak var pagesView: UIView!
    @IBOutlet weak var followingView: UIView!
    @IBOutlet weak var followersView: UIView!
    @IBOutlet weak var pagesNumberLabel: UILabel!
    @IBOutlet weak var pagesLabel: UILabel!
    @IBOutlet weak var followersNumberLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingNumberLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var featuredCollectionView: UICollectionView!
    @IBOutlet weak var featuredCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        featuredCollectionView.register(UINib.init(nibName: "FeaturedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FeaturedCollectionViewCell")
        featuredCollectionView.delegate = self
        featuredCollectionView.dataSource = self
        setupViews()
    }
    
    func setupViews() {
        for view in [contentView, featuredCollectionView] {
            view?.setBorder()
        }
        scrollView.backgroundColor = UIColor(red: 235.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        for view in [avatarImageView, avatarBackView] {
            view?.layer.cornerRadius = (view?.frame.size.width)! / 2
        }
        avatarImageView.image = #imageLiteral(resourceName: "AvatarImage")
        avatarImageView.clipsToBounds = true
        avatarImageView.layer.borderWidth = 6
        avatarImageView.layer.borderColor = UIColor.white.cgColor
        avatarBackView.layer.borderWidth = 1
        avatarBackView.layer.borderColor = UIColor.gray.cgColor
        
        nickLabel.text = "Vika@"
        nickLabel.font = UIFont.boldSystemFont(ofSize: 20)
        nameLabel.text = "Victoria Colt"
        nameLabel.textColor = UIColor.lightGray
        
        followButton.setTitle("+ Follow", for: .normal)
        followButton.styledButton()
        shareButton.setTitle("Share in Social", for: .normal)
        shareButton.styledButtonInverse()
        
        infoStackView.distribution = .fillEqually
        for view in [pagesView, followersView, followingView] {
            view?.setBorder()
        }
        
        for label in [pagesNumberLabel, followersNumberLabel, followingNumberLabel] {
            label?.font = UIFont.boldSystemFont(ofSize: 17)
        }
        pagesNumberLabel.text = "180"
        pagesLabel.text = "Pages"
        followersNumberLabel.text = "1244"
        followersLabel.text = "Followers"
        followingNumberLabel.text = "12455"
        followingLabel.text = "Following"
        
        for label in [pagesLabel, followersLabel, followingLabel, questionLabel, answerLabel] {
            label?.textColor = UIColor.lightGray
        }
        questionLabel.text = "Do You Want To Go Out With Me?"
        answerLabel.text = "(A) YES (B) A (C) B."
        
        featuredCollectionView.isScrollEnabled = false
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupFeaturedCollectionFlowLoyout()
    }
    
    func setupFeaturedCollectionFlowLoyout() {
        featuredCollectionViewFlowLayout.itemSize = CGSize(width: (featuredCollectionView.frame.size.width - 60) / 3.3, height: (featuredCollectionView.frame.size.height) / 3.3)
        featuredCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    }
}

extension DesignViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = featuredCollectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCollectionViewCell", for: indexPath) as! FeaturedCollectionViewCell
        cell.setupCell(indexPath.row)
        return cell
    }
}
