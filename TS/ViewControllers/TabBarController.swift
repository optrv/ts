//
//  TabBarController.swift
//  TS
//
//  Created by Garazd on 1/31/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let translateVC = TranslateViewController()
        
        let gifVC = GifViewController()
        
        let designVC = DesignViewController()
        let designNC = UINavigationController(rootViewController: designVC)
        designNC.navigationBar.barTintColor = UIColor.black
        designNC.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        designVC.title = "Vika@"
        
        setViewControllers([translateVC, gifVC, designNC], animated: false)
        
        translateVC.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
        gifVC.tabBarItem = UITabBarItem(tabBarSystemItem: .mostViewed, tag: 1)
        designVC.tabBarItem = UITabBarItem(tabBarSystemItem: .topRated, tag: 2)
    }
}
