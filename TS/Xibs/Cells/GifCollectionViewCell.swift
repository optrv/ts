//
//  GifCollectionViewCell.swift
//  TS
//
//  Created by Garazd on 2/4/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit
import Nuke
import NukeFLAnimatedImagePlugin
import FLAnimatedImage

class GifCollectionViewCell: UICollectionViewCell {
    
    let imageView: AnimatedImageView
    let activityIndicator: UIActivityIndicatorView
    
    override init(frame: CGRect) {
        imageView = AnimatedImageView()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(white: 235.0 / 255.0, alpha: 1.0)
        
        imageView.imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        contentView.addSubview(imageView)
        imageView.frame = contentView.bounds
        imageView.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
        
        contentView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        contentView.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        contentView.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: 0.0))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.imageView.image = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
