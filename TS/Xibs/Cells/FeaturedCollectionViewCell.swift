//
//  FeaturedCollectionViewCell.swift
//  TS
//
//  Created by Garazd on 2/3/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit

class FeaturedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var featuredView: UIView!
    @IBOutlet weak var featuredLabel: UILabel!
    @IBOutlet weak var featuredImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    func setupViews() {
        featuredView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        featuredView.isHidden = true
        featuredLabel.text = "Featured"
        featuredLabel.textColor = UIColor.white
        featuredLabel.font = UIFont.systemFont(ofSize: 12)
        featuredImage.image = #imageLiteral(resourceName: "Diamond")
    }
    
    func setupCell(_ index: Int) {
        if index == 0 {
            featuredView.isHidden = false
        }
        cellImageView.image = #imageLiteral(resourceName: "CellImage")
        
        cellLabel.text = "Some of the girls nicer and more..."
        cellLabel.numberOfLines = 2
        cellLabel.lineBreakMode = .byTruncatingTail
        cellLabel.font = UIFont.systemFont(ofSize: 10)
        cellLabel.textColor = UIColor.gray
    }
}
