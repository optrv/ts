//
//  AppDelegate.swift
//  TS
//
//  Created by Garazd on 1/31/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let mainVC = TabBarController()
        let mainNC = UINavigationController(rootViewController: mainVC)
        window?.rootViewController = mainNC
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        CoreDataController.saveContext()
    }
}
