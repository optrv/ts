//
//  Translate+CoreDataProperties.swift
//  TS
//
//  Created by Garazd on 2/5/18.
//  Copyright © 2018 Garazd. All rights reserved.
//
//

import Foundation
import CoreData


extension Translate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Translate> {
        return NSFetchRequest<Translate>(entityName: "Translate")
    }

    @NSManaged public var inputText: String?
    @NSManaged public var outputText: String?

}
