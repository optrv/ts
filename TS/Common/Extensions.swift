//
//  Extensions.swift
//  TS
//
//  Created by Garazd on 1/31/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import UIKit

extension UITextView {
    func styled() {
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
    }
}

extension UIButton {
    private func common() {
        self.layer.cornerRadius = 18
        self.titleLabel?.font = UIFont.systemFont(ofSize: 13)
    }
    func styledButton() {
        common()
        self.backgroundColor = UIColor(red: 35.0/255.0, green: 181.0/255.0, blue: 188.0/255.0, alpha: 1.0)
        self.setTitleColor(UIColor.white, for: .normal)
    }
    func styledButtonInverse() {
        common()
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red: 35.0/255.0, green: 181.0/255.0, blue: 188.0/255.0, alpha: 1.0).cgColor
        self.setTitleColor(UIColor.black, for: .normal)
    }
}

extension UIView {
    func setBorder() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
}
