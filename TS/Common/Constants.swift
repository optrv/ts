//
//  Constants.swift
//  TS
//
//  Created by Oleksandr Petrov on 2/1/18.
//  Copyright © 2018 Garazd. All rights reserved.
//

import Foundation

struct Constants {
    
    enum LanguageSelector: String {
        case en = "en"
        case ua = "uk"
    }
    
    struct Authorization {
        static let key = "6b0b30fe15e44359be47c14f5a3a6f67"
        static let header = "Ocp-Apim-Subscription-Key"
    }
    
    struct URL {
        static let baseURL = "https://api.microsofttranslator.com/V2/Http.svc/Translate"
    }
}
